//creating variables
rainfallInput = 0
totalRain = 0
month=1
//console.log title
console.log("rainfall for a year")

//allowing user to input rainfall for each month
while (month<=12) { 
rainfallInput = Number(prompt(`Welcome to rainfall calculator for a year.\nEnter rainfall for month ${month}:`))
    console.log(`Rainfall for month ${month}: ${rainfallInput}mm`)
    month++
    totalRain = totalRain + rainfallInput
}

console.log(`Total rainfall: ${totalRain}mm`)