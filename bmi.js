//Indicating Variables
let weight = 0
let height = 0
let bmi = 0
//Allowing user to input weight and height
weight = Number(prompt("Welcome to Body Mass Index Calculator.\nEnter your weight (kg):"))
height = Number(prompt("enter your height (cm):"))


//Title in the console log
console.log("Body Mass Index Calculator")
console.log("--------------------")

//showing the person what values they inputted
console.log(`Your weight: ${weight} kg`)
console.log(`Your height: ${height} cm`)
console.log("--------------------")

//Body mass index
bmi = (weight / (height/100)**2).toFixed(1)

//outputting body mass index
console.log(`Body Mass Index: ${bmi}`)
console.log(`-----------------------`)

//if statements to determine how obese someone is

if (bmi>=30) {

    console.log("BMI category: obese")
    
}

else if (bmi>=25 && bmi<30) {

    console.log("BMI category: overweight")
    
}

else if (bmi>=18.5 && bmi<25) {

    console.log("BMI category: normal weight")
    
}

else if (bmi<18.5) {

    console.log("BMI category: underweight")
}

else {

        console.log("Invalid Input")
}


